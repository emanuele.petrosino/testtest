//
//  ContentView.swift
//  test
//
//  Created by Emanuele Petrosino on 05/12/2019.
//  Copyright © 2019 EP. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, World!")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
